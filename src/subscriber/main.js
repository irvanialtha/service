`use strict`;

module.exports = {
    data: (data) =>{
        amqp.publish(data.source,data.topic,{... data});
    }
}