
'use strict';

global.basedir = process.env.PWD;

try {
    global.environment = require(`${basedir}/config/env.json`);
} catch(error) {
    throw new Error(`${basedir}/config/env.json is not found.`)
}

const 
    fs  = require(`fs`),
    obj = {};

fs.readdirSync(`${__dirname}/collections`).forEach((file) => {
    const src = require(`${__dirname}/collections/${file}`);

    obj[file.replace(/.js|.ts/g, ``)] = src;
});

 module.exports = { ... obj };