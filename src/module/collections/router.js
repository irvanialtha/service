/**
 * @package     : @hig/router
 * @author      : Harry Surya Kurniawan <harrysuryak@gmail.com>
 * @description : Route loader
 */

`use strict`;

const
    fs        = require(`fs`),
    express   = require(`express`),
    busboy    = require(`express-busboy`),
    useragent = require(`express-useragent`);

const
    http_request = require(`${basedir}/src/middleware/http_request`)
// Create new route
const createroute = () => {
    const route = express.Router();
    route.use(http_request);
    route.use(express.json());
    route.use(useragent.express());
    route.use(express.urlencoded({ extended : true }));

    busboy.extend(route, {
        upload : true,
        path   : `${basedir}/protected/`,
        mimeTypeLimit : [
            /* .jpg & .jpeg */ `image/jpeg`,
            /* .png         */ `image/png`,
            /* .xls         */ `application/vnd.ms-excel`,
            /* .ppt         */ `application/vnd.ms-powerpoint`,
            /* .doc         */ `application/msword`,
            /* .csv         */ `text/csv`,
            /* .xlsx        */ `application/vnd.openxmlformats-officedocument.spreadsheetml.sheet`,
            /* .pptx        */ `application/vnd.openxmlformats-officedocument.presentationml.presentation`,
            /* .docx        */ `application/vnd.openxmlformats-officedocument.wordprocessingml.document`,
            /* .pdf         */ `application/pdf`,
            /* .zip         */ `application/zip`,
            /* .rar         */ `application/x-rar-compressed`,
            /* .7z          */ `application/x-7z-compressed`,
            /* .txt         */ `text/plain`
        ]
    });

    return route;
};

module.exports = () => {
    const
        app   = express(),
        route = createroute()
        
    app.listen(environment.app.port, environment.app.host);

    fs.readdirSync(`${basedir}/src/router`).forEach((file) => {
        const
            newroute = createroute(),
            name     = file.replace(/.js|.ts/g, ``).toLowerCase(),
            source   = require(`${basedir}/src/router/${file}`);

        if(file === `main.js`) {
            app.use(`/`, source(newroute));
        }

        app.use(`/${name}`, source(newroute));
    });

    return app;
};
