`use strict`;

module.exports = () => {
    if (global.amqp) {
        global.amqp.on('message',(data)=>{
            const
                target     = data.topic.split('.'),
                requestid  = data.requestid ? data.requestid : null;
            let 
                request, 
                response,
                next;
            
            if(environment.app.type == 'service'){
                try {
                    subscriber[target[0]][target[1]](data);   
                } catch (error) {
                    console.log(error)
                }
            }

            if(requestid && http_request.hasOwnProperty(requestid)) {
                request  = http_request[requestid].request;
                response = http_request[requestid].response;
                next     = http_request[requestid].next;
    
                try {
                    subscriber[target[0]][target[1]](request, response, data);   
                } catch (error) {
                    response.send(error.message)
                }
            }
        })
    }
   

    if (global.mqtt) {
        global.mqtt.on('message',(topic, data)=>{

            const
                target     = data.topic.split('.'),
                requestid  = data.requestid ? data.requestid : null;
            let 
                request, 
                response,
                next;

            if(environment.app.type == 'service'){
                try {
                    subscriber[target[0]][target[1]](data);   
                } catch (error) {
                    console.log(error)
                }
            }
            if(requestid && http_request.hasOwnProperty(requestid)) {
                request  = http_request[requestid].request;
                response = http_request[requestid].response;
                next     = http_request[requestid].next;
    
                try {
                    subscriber[target[0]][target[1]](request, response, data);   
                } catch (error) {
                    response.send(error.message)
                }
            }
        })
    }
    


}