/**
 * @file        : redis.js
 * @author      : Harry Surya Kurniawan <harry.surya@elnusa.co.id>
 * @description : Redis Connector
 */

`use strict`;

const redis = require(`redis`);

let client;

const main = {
    init : (host, port, db) => {
        host = host || environment.cache.host;
        port = port || environment.cache.port || 6379;
        db   = db   || environment.cache.db   || 0;

        return new Promise((resolve, reject) => {
            client = redis.createClient({
                host : host,
                port : port,
                connect_timeout : 2000
            });

            client.on(`connect`, () => {
                client.select(db);
                resolve(client);
            });

            client.on(`error`, (error) => {
                reject(error);
            });
        });
    },

    set : (key, data, ...args) => {
        let callback, ttl = 86400; 

        if(args[0] && args[0].constructor === Number) {
            ttl      = 60 * 60 * parseFloat(args[0]);
            callback = args[1] ? args[1] : null;
        }
        else if(args[0] && args[0].constructor === Function) {
            callback = args[0];
        }

        client.set(key, data, `EX`, ttl, callback);
    },

    get : (key, callback, stringify = true) => {
        client.get(key, (err, reply) => {
            if(stringify) {
                try {
                    reply = JSON.parse(reply);
                }
                catch(error) {
                    // 
                }
            }

            callback(err, reply);
        });
    },

    del : (key, callback) => {
        if(key.includes(`*`)) {
            client.keys(key, (err, result) => {
                if(err) {
                    callback(err, null);
                    return;
                }

                try {
                    for(let i in result) {
                        client.del(result[i]);

                        if(parseInt(i) === result.length - 1) {
                            callback(null, 1);
                        }
                    }
                }
                catch(err) {
                    callback(err, null);
                }
            });

            return;
        }

        client.del(key, callback);
    },

    keys : (key, callback) => {
        client.keys(key, callback);
    },

    select : (db, callback) => {
        client.select(db, callback);
    }
};

module.exports = (host, port, db) => {
    return new Promise(async (resolve, reject) => {
        await main.init(host, port, db).then((conn) => {
            const set = [
                'hgetall', 'hexists', 'hmset', 'hmget', 'hkeys', 'hvals', 'hget', 'hset', 'hdel',
                'mget', 'mset', 'set', 'del', 'exists', 'lpush', 'lrange',
                'zrange', 'zrem', 'zadd', 'zrangebyscore', 'zrevrangebyscore',
                'expire', 'incrby', 'end', 'select', 'keys'
            ];

            const customfn = {};

            set.map((fn) => {
                customfn[fn] = (...args) => {
                    return conn[fn](args);
                };
            });

            customfn.set    = main.set;
            customfn.get    = main.get;
            customfn.del    = main.del;
            customfn.keys   = main.keys;
            customfn.select = main.select;

            global.cache = customfn;

            resolve(customfn);
        }).catch((error) => {
            reject(error);
        });
    });
};