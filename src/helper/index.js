
'use strict';

const 
    fs  = require(`fs`),
    obj = {};

fs.readdirSync(`${__dirname}/collections`).forEach((file) => {
    const src = require(`${__dirname}/collections/${file}`);

    obj[file.replace(/.js|.ts/g, ``)] = src;
});

 module.exports = { ... obj };