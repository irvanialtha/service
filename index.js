`use strict`;

const 
    { service, amqp, mqtt, subscriber } = require(`./src/module`);

let start = () => {
    return new Promise(async (resolve, reject) => {
        resolve({
            service : await service(),
            amqp    : await amqp(),
            mqtt    : await mqtt()
        })
     }).then(async (data) => {
        subscriber();
     });
};

start()